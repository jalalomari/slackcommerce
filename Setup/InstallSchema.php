<?php
namespace Moogento\SlackCommerce\Setup;

use Magento\Framework\DB\Ddl\Table;
use Magento\Framework\Setup\InstallSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;
use Magento\Framework\DB\Adapter\AdapterInterface;
use Moogento\SlackCommerce\Model\NotificationInterface;

class InstallSchema implements InstallSchemaInterface
{

    /**
     * Installs DB schema for a module
     *
     * @param SchemaSetupInterface $setup
     * @param ModuleContextInterface $context
     *
     * @return void
     */
    public function install(
        SchemaSetupInterface $setup,
        ModuleContextInterface $context
    ) {
        $installer = $setup;

        $installer->startSetup();
        $context->getVersion();

        $table = $installer->getConnection()->newTable(
            $installer->getTable(SetupContextInterface::QUEUE_TABLE)
        )->addColumn(
            'queue_id',
            Table::TYPE_SMALLINT,
            null,
            ['identity' => true, 'nullable' => false, 'primary' => true],
            'Queue ID'
        )->addColumn(
            NotificationInterface::FIELD_EVENT_KEY,
            Table::TYPE_TEXT,
            255,
            ['nullable' => false],
            'Event Key'
        )->addColumn(
            NotificationInterface::FIELD_REFERENCE_ID,
            Table::TYPE_INTEGER,
            10,
            ['nullable' => false, 'unsigned' => true],
            'Reference ID'
        )->addColumn(
            NotificationInterface::FIELD_ADDITIONAL_DATA,
            Table::TYPE_TEXT,
            null,
            ['nullable' => true, 'default' => null]
        )->addColumn(
            NotificationInterface::FIELD_STATUS,
            Table::TYPE_SMALLINT,
            1,
            ['nullable' => false, 'unsigned' => true, 'default' => 0]
        )->addColumn(
            NotificationInterface::FIELD_STATUS_MESSAGE,
            Table::TYPE_TEXT,
            null,
            ['nullable' => true, 'default' => null]
        )->addColumn(
            NotificationInterface::FIELD_DATE,
            Table::TYPE_TIMESTAMP,
            null,
            ['nullable' => true, 'default' => '0000-00-00 00:00:00']
        )->addColumn(
            NotificationInterface::FIELD_CRON_ID,
            Table::TYPE_TEXT,
            255,
            ['nullable' => true, 'default' => null]
        )->addIndex(
            $setup->getIdxName(
                $installer->getTable(SetupContextInterface::QUEUE_TABLE),
                [NotificationInterface::FIELD_STATUS],
                AdapterInterface::INDEX_TYPE_INDEX
            ),
            [NotificationInterface::FIELD_STATUS],
            ['type' => AdapterInterface::INDEX_TYPE_INDEX]
        )->addIndex(
            $setup->getIdxName(
                $installer->getTable(SetupContextInterface::QUEUE_TABLE),
                [NotificationInterface::FIELD_CRON_ID],
                AdapterInterface::INDEX_TYPE_INDEX
            ),
            [NotificationInterface::FIELD_CRON_ID],
            ['type' => AdapterInterface::INDEX_TYPE_INDEX]
        )->addIndex(
            $setup->getIdxName(
                $installer->getTable(SetupContextInterface::QUEUE_TABLE),
                [NotificationInterface::FIELD_DATE],
                AdapterInterface::INDEX_TYPE_INDEX
            ),
            [NotificationInterface::FIELD_DATE],
            ['type' => AdapterInterface::INDEX_TYPE_INDEX]
        )->setComment(
            'SlackCommerce Queue Table'
        );
        $installer->getConnection()->createTable($table);

        $ipFailsTable = $installer->getConnection()->newTable(
            $installer->getTable(SetupContextInterface::FAILS_IP_TABLE)
        )->addColumn(
            'id',
            Table::TYPE_SMALLINT,
            null,
            ['identity' => true, 'nullable' => false, 'primary' => true],
            'ID'
        )->addColumn(
            'ip',
            Table::TYPE_VARBINARY,
            15,
            ['nullable' => false],
            'IP'
        )->addColumn(
            'fails',
            Table::TYPE_INTEGER,
            11,
            ['nullable' => false, 'unsigned' => true, 'default' => 0]
        )->addColumn(
            'fails_per_day',
            Table::TYPE_INTEGER,
            11,
            ['nullable' => false, 'unsigned' => true, 'default' => 0]
        )->addIndex(
            $setup->getIdxName(
                $installer->getTable(SetupContextInterface::FAILS_IP_TABLE),
                ['ip'],
                AdapterInterface::INDEX_TYPE_UNIQUE
            ),
            ['ip'],
            ['type' => AdapterInterface::INDEX_TYPE_UNIQUE]
        );
        $installer->getConnection()->createTable($ipFailsTable);

        $targetFailsTable = $installer->getConnection()->newTable(
            $installer->getTable(SetupContextInterface::FAILS_TARGET_TABLE)
        )->addColumn(
            'id',
            Table::TYPE_SMALLINT,
            null,
            ['identity' => true, 'nullable' => false, 'primary' => true]
        )->addColumn(
            'target',
            Table::TYPE_TEXT,
            255,
            ['nullable' => true, 'default' => null]
        )->addColumn(
            'fails_per_day',
            Table::TYPE_INTEGER,
            11,
            ['nullable' => false, 'unsigned' => true, 'default' => 0]
        )->addIndex(
            $setup->getIdxName(
                $installer->getTable(SetupContextInterface::FAILS_TARGET_TABLE),
                ['target'],
                AdapterInterface::INDEX_TYPE_UNIQUE
            ),
            ['target'],
            ['type' => AdapterInterface::INDEX_TYPE_UNIQUE]
        );
        $installer->getConnection()->createTable($targetFailsTable);

        $setup->endSetup();
    }
}
