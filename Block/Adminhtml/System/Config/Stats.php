<?php
namespace Moogento\SlackCommerce\Block\Adminhtml\System\Config;

use Moogento\SlackCommerce\Helper\Config;

class Stats extends CustomGroup
{
    protected $_template = 'system/config/stats.phtml';

    protected $_statsList
        = [
            Config::KEY_STATS => [
                Config::SUBTYPE_SEND_TYPE,
                Config::SUBTYPE_CUSTOM_CHANNEL,
                Config::SUBTYPE_COLORIZE,
                Config::SUBTYPE_COLOR,
            ],
            Config::KEY_QTY_ORDERS,
            Config::KEY_TOTAL_REVENUE,
            Config::KEY_QTY_PRODUCTS,
            Config::KEY_AVG_PRODUCTS_ORDER,
            Config::KEY_AVG_REVENUE_ORDER,
            Config::KEY_HOUR,
            Config::KEY_DAILY_STATS,
            Config::KEY_WEEKLY_STATS,
            Config::KEY_DAY,
        ];

    public function getValues()
    {
        $stats = [];
        foreach ($this->_statsList as $key => $data) {
            if (is_array($data)) {
                foreach ($data as $subtype) {
                    $stats[$key . '_' . $subtype]
                        = $this->_configHelper
                        ->getConfigModel()
                        ->getConfigDataValue(
                            $this->_configHelper->buildPath(
                                $key,
                                $subtype,
                                Config::SECTION_STATS
                            )
                        );
                }
            } else {
                $stats[$data] = $this->_configHelper
                    ->getConfigModel()
                    ->getConfigDataValue(
                        $this->_configHelper->buildPath(
                            $data,
                            false,
                            Config::SECTION_STATS
                        )
                    );
            }
        }
        return $this->_jsonHelper->jsonEncode($stats);
    }

    public function getWeekdays()
    {
        return $this->_objectManager->get(
            '\Magento\Config\Model\Config\Source\Locale\Weekdays'
        )->toOptionArray();
    }
}
