<?php
namespace Moogento\SlackCommerce\Block\Adminhtml\System\Config;

use Moogento\SlackCommerce\Helper\Config;

class Security extends CustomGroup
{
    protected $_template = 'system/config/security.phtml';

    protected $_securityList
        = [
            Config::KEY_REPORT    => [
                Config::SUBTYPE_SEND_TYPE,
                Config::SUBTYPE_CUSTOM_CHANNEL,
                Config::SUBTYPE_COLORIZE,
                Config::SUBTYPE_COLOR,
            ],
            Config::KEY_IMMEDIATE => [
                Config::SUBTYPE_SEND_TYPE,
                Config::SUBTYPE_CUSTOM_CHANNEL,
                Config::SUBTYPE_COLORIZE,
                Config::SUBTYPE_COLOR,
            ],
            Config::KEY_HOUR,
            Config::KEY_TOTAL_FAILS,
            Config::KEY_COUNT_IP_FAILS,
            Config::KEY_COUNT_TARGET_FAILS,
            Config::KEY_SKIP_NO_FAILS,
        ];

    public function getValues()
    {
        $security = [];
        foreach ($this->_securityList as $key => $data) {
            if (is_array($data)) {
                foreach ($data as $subtype) {
                    $security[$key . '_' . $subtype]
                        = $this->_configHelper
                        ->getConfigModel()
                        ->getConfigDataValue(
                            $this->_configHelper->buildPath(
                                $key,
                                $subtype,
                                Config::SECTION_SECURITY
                            )
                        );
                }
            } else {
                $security[$data] = $this->_configHelper
                    ->getConfigModel()
                    ->getConfigDataValue(
                        $this->_configHelper->buildPath(
                            $data,
                            false,
                            Config::SECTION_SECURITY
                        )
                    );
            }
        }

        return $this->_jsonHelper->jsonEncode($security);
    }
}
