<?php
namespace Moogento\SlackCommerce\Block\Adminhtml\System\Config\Form\Button;

/**
 * @method \Magento\Config\Block\System\Config\Form getForm
 * @method Test setElement
 */
class Test extends \Magento\Config\Block\System\Config\Form\Field
{
    /**
     * @param \Magento\Framework\Data\Form\Element\AbstractElement $element
     *
     * @return string
     */
    protected function _getElementHtml(
        \Magento\Framework\Data\Form\Element\AbstractElement $element
    ) {
        $this->setElement($element);
        /** @var \Magento\Backend\Block\Widget\Button $buttonBlock */
        $buttonBlock = $this->getForm()->getLayout()->createBlock(
            'Magento\Backend\Block\Widget\Button'
        );

        $url  = $buttonBlock->getUrl("moogento_slackcommerce/test/send");
        $data = [
            'id'      => 'moogento_slackcommerce_test',
            'label'   => __('Send test message'),
            'onclick' => "setLocation('" . $url . "')",
        ];

        $html = $buttonBlock->setData($data)->toHtml();
        return $html;
    }
}
