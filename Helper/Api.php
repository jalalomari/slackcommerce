<?php
namespace Moogento\SlackCommerce\Helper;

use Exception;
use Magento\Framework\App\Helper\AbstractHelper;

class Api extends AbstractHelper
{
    /**
     * @var \Magento\Framework\HTTP\ZendClientFactory
     */
    protected $_httpClientFactory;

    public function __construct(
        \Magento\Framework\App\Helper\Context $context,
        \Magento\Framework\HTTP\ZendClientFactory $httpClientFactory
    ) {
        $this->_httpClientFactory = $httpClientFactory;
        parent::__construct($context);
    }

    public function send($data)
    {
        $webHookUrl = $this->scopeConfig->getValue(
            'moogento_slackcommerce/general/webhook_url'
        );
        if (!$webHookUrl) {
            throw new Exception(__('Slack Webhook URL not defined'));
        }

        if (!isset($data['channel']) || !$data['channel']) {
            $data['channel'] = $this->scopeConfig->getValue(
                'moogento_slackcommerce/general/default_channel'
            );
        }

        if (!$data['channel']) {
            throw new Exception(__('Slack Default Channel is not defined'));
        }
        $postData = [
            'payload' => json_encode($data),
        ];

        /** @var \Magento\Framework\HTTP\ZendClient $client */
        $client = $this->_httpClientFactory->create();
        $client->setUri($webHookUrl);
        $client->setConfig(['maxredirects' => 0, 'timeout' => 30]);

        $client->setParameterPost($postData);
        $client->setMethod(\Zend_Http_Client::POST);

        $response = $client->request();
        $responseBody = $response->getBody();

        if (trim($responseBody) == 'ok') {
            return true;
        }

        return $responseBody;
    }
}
