<?php
namespace Moogento\SlackCommerce\Helper;

use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Framework\App\Helper\Context;
use Magento\Framework\UrlInterface;
use Magento\Store\Model\StoreManagerInterface;

class Config extends AbstractHelper
{
    const GROUP = 'moogento_slackcommerce';
    const SECTION_NOTIFICATIONS = 'notifications';
    const SECTION_SECURITY = 'security';
    const SECTION_GENERAL = 'general';
    const SECTION_STATS = 'stats';

    const KEY_STATS = 'stats';
    const KEY_REPORT = 'report';
    const KEY_IMMEDIATE = 'immediate';
    const KEY_WEBHOOK_URL = 'webhook_url';
    const KEY_DAILY_STATS = 'daily_stats';
    const KEY_WEEKLY_STATS = 'weekly_stats';
    const KEY_DAY = 'day';
    const KEY_HOUR = 'hour';
    const KEY_ICON = 'icon';
    const KEY_QTY_ORDERS = 'qty_orders';
    const KEY_TOTAL_REVENUE = 'total_revenue';
    const KEY_QTY_PRODUCTS = 'qty_products';
    const KEY_AVG_PRODUCTS_ORDER = 'avg_products_order';
    const KEY_AVG_REVENUE_ORDER = 'avg_revenue_order';
    const KEY_DEFAULT_CHANNEL = 'default_channel';
    const KEY_SKIP_NO_FAILS = 'skip_no_fails';
    const KEY_TOTAL_FAILS = 'total_fails';
    const KEY_COUNT_IP_FAILS = 'count_ip_fails';
    const KEY_COUNT_TARGET_FAILS = 'count_target_fails';
    const KEY_WEEKS_CHANGE = 'weeks_change';

    const SUBTYPE_COLORIZE = 'colorize';
    const SUBTYPE_COLOR = 'color';
    const SUBTYPE_SEND_TYPE = 'send_type';
    const SUBTYPE_CUSTOM_CHANNEL = 'custom_channel';

    const SEND_TYPE_DEFAULT = 'default';
    const SEND_TYPE_CUSTOM = 'custom';

    const NOTIFICATION_FLAG = 'process_notifications';

    /** @var \Magento\Config\Model\ResourceModel\Config */
    protected $_configSaver;

    /** @var StoreManagerInterface */
    protected $_storeManager;

    /** @var \Magento\Config\Model\Config */
    protected $_configModel;

    public function __construct(
        Context $context,
        \Magento\Config\Model\ResourceModel\Config $configSaver,
        \Magento\Config\Model\Config $configModel,
        StoreManagerInterface $storeManager
    ) {
        $this->_configSaver = $configSaver;
        $this->_storeManager = $storeManager;
        $this->_configModel = $configModel;
        parent::__construct($context);
    }

    public function buildPath(
        $key,
        $subType = false,
        $section = self::SECTION_NOTIFICATIONS,
        $group = self::GROUP
    ) {
        return $group . '/' . $section . '/' . $key . ($subType ? '_' . $subType
            : '');
    }

    public function getValue(
        $key,
        $subType = false,
        $section = self::SECTION_NOTIFICATIONS,
        $group = self::GROUP
    ) {
        return $this->scopeConfig->getValue(
            $group . '/' . $section . '/' . $key . ($subType ? '_' . $subType
                : '')
        );
    }

    public function getSecurityValue(
        $key,
        $subType = false,
        $section = self::SECTION_SECURITY,
        $group = self::GROUP
    ) {
        return $this->scopeConfig->getValue(
            $group . '/' . $section . '/' . $key . ($subType ? '_' . $subType
                : '')
        );
    }

    public function isSetFlag(
        $key,
        $subType = false,
        $section = self::SECTION_NOTIFICATIONS,
        $group = self::GROUP
    ) {
        return $this->scopeConfig->isSetFlag(
            $group . '/' . $section . '/' . $key . ($subType ? '_' . $subType
                : '')
        );
    }

    public function shouldSend($key, $section = self::SECTION_NOTIFICATIONS)
    {
        $sendType = $this->getSendType($key, $section);
        return $sendType == self::SEND_TYPE_DEFAULT
               || $sendType == self::SEND_TYPE_CUSTOM;
    }

    public function getSendType($key, $section = self::SECTION_NOTIFICATIONS)
    {
        return $this->getValue($key, 'send_type', $section);
    }

    public function getWebHookUrl()
    {
        return $this->getValue(
            self::KEY_WEBHOOK_URL,
            false,
            self::SECTION_GENERAL
        );
    }

    public function getDefaultChannel()
    {
        return $this->getValue(
            self::KEY_DEFAULT_CHANNEL,
            false,
            self::SECTION_GENERAL
        );
    }

    public function getRemoteAddress()
    {
        return $this->_remoteAddress;
    }

    public function save(
        $value,
        $key,
        $section = self::SECTION_GENERAL,
        $group = self::GROUP
    ) {
        $this->_configSaver->saveConfig(
            $group . '/' . $section . '/' . $key,
            $value,
            'default',
            0
        );
        return $this;
    }

    public function getRequest()
    {
        return $this->_getRequest();
    }

    public function getDefaultIcon()
    {
        return $this->_storeManager->getStore()->getBaseUrl(
            UrlInterface::URL_TYPE_MEDIA
        )
               . 'moogento/slack/moogento_logo_small.png';
    }

    /**
     * @return \Magento\Config\Model\Config
     */
    public function getConfigModel()
    {
        return $this->_configModel;
    }
}
