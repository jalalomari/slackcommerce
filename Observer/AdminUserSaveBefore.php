<?php
namespace Moogento\SlackCommerce\Observer;

use Magento\Framework\Event\Observer;
use Moogento\SlackCommerce\Helper\Config;

class AdminUserSaveBefore extends ObserverAbstract
{

    /**
     * @param Observer $observer
     *
     * @return void
     */
    public function execute(Observer $observer)
    {
        /** @var \Magento\User\Model\User $user */
        $user = $observer->getDataObject();
        if ($user->isObjectNew()) {
            $user->setData(Config::NOTIFICATION_FLAG, true);
        }
    }
}
