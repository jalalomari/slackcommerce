<?php
namespace Moogento\SlackCommerce\Observer\SalesOrder;

use Magento\Framework\Event\Observer;
use Moogento\SlackCommerce\Helper\Config;
use Moogento\SlackCommerce\Observer\ObserverAbstract;

class InvoiceSaveBefore extends ObserverAbstract
{

    /**
     * @param Observer $observer
     *
     * @return void
     */
    public function execute(Observer $observer)
    {
        /** @var \Magento\Sales\Model\Order\Invoice $invoice */
        $invoice = $observer->getInvoice();
        if ($invoice->isObjectNew()) {
            $invoice->setData(Config::NOTIFICATION_FLAG, true);
        }
    }
}
