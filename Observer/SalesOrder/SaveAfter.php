<?php
namespace Moogento\SlackCommerce\Observer\SalesOrder;

use Magento\Framework\Event\Observer;
use Magento\Sales\Model\Order;
use Moogento\SlackCommerce\Helper\Config;
use Moogento\SlackCommerce\Model\Queue;
use Moogento\SlackCommerce\Observer\ObserverAbstract;

class SaveAfter extends ObserverAbstract
{

    /**
     * @param Observer $observer
     *
     * @return void
     */
    public function execute(Observer $observer)
    {
        /** @var \Magento\Sales\Model\Order $order */
        $order = $observer->getOrder();

        if ($order->getData(Config::NOTIFICATION_FLAG)
            && $this->_configHelper->shouldSend(Queue::KEY_NEW_ORDER)
        ) {
            /** @var \Magento\Sales\Model\Order $order */
            $order = $observer->getOrder();

            $queue = $this->_objectManager->create(
                '\Moogento\SlackCommerce\Model\Queue'
            );
            $queue->setData(
                [
                    'event_key'    => Queue::KEY_NEW_ORDER,
                    'reference_id' => $order->getId(),
                    'date'         => $this->_dateTime->gmtDate("Y-m-d H:i:s"),
                ]
            );
            try {
                $queue->save();
            } catch (\Exception $e) {
                $this->_logger->warning($e);
            }
        }

        if ($order->getOrigData(Order::STATUS)
            &&
            $order->getOrigData(Order::STATUS) != $order->getData(Order::STATUS)
            && $this->_configHelper->shouldSend(
                Queue::KEY_NEW_STATUS . '_' . $order->getData(Order::STATUS)
            )
        ) {

            $queue = $this->_objectManager->create(
                '\Moogento\SlackCommerce\Model\Queue'
            );
            $queue->setData(
                [
                    'event_key'    => Queue::KEY_NEW_STATUS . '_'
                                      . $order->getData(Order::STATUS),
                    'reference_id' => $order->getId(),
                    'date'         => $this->_dateTime->gmtDate("Y-m-d H:i:s"),
                ]
            );
            try {
                $queue->save();
            } catch (\Exception $e) {
                $this->_logger->warning($e);
            }
        }
    }
}
