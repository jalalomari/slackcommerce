<?php
namespace Moogento\SlackCommerce\Observer;

use Magento\Framework\Event\Observer;
use Moogento\SlackCommerce\Helper\Config;
use Moogento\SlackCommerce\Model\Queue;

class AdminUserSaveAfter extends ObserverAbstract
{

    /**
     * @param Observer $observer
     *
     * @return void
     */
    public function execute(Observer $observer)
    {
        /** @var \Magento\User\Model\User $user */
        $user = $observer->getDataObject();

        if ($user->getData(Config::NOTIFICATION_FLAG)
            && $this->_configHelper->shouldSend(Queue::KEY_NEW_BACKEND_ACCOUNT)
        ) {
            $user->setData(Config::NOTIFICATION_FLAG, false);

            $queue = $this->_objectManager->create(
                '\Moogento\SlackCommerce\Model\Queue'
            );
            $queue->setData(
                [
                    'event_key' => Queue::KEY_NEW_BACKEND_ACCOUNT,
                    'reference_id' => $user->getId(),
                    'date' => $this->_dateTime->gmtDate("Y-m-d H:i:s"),
                ]
            );
            try {
                $queue->save();
            } catch (\Exception $e) {
                $this->_logger->warning($e);
            }
        }
    }
}
