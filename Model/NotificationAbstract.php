<?php
namespace Moogento\SlackCommerce\Model;

use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\ObjectManagerInterface;
use Magento\Framework\UrlInterface;
use Magento\Store\Model\App\Emulation;
use Magento\Store\Model\StoreManagerInterface;
use Moogento\SlackCommerce\Helper\Config;

abstract class NotificationAbstract extends \Magento\Framework\DataObject
    implements NotificationInterface
{
    protected $_referenceModel = false;
    protected $_referenceObject = null;
    protected $_scopeConfig;

    /** @var  ObjectManagerInterface */
    protected $_objectManager;

    /** @var Emulation */
    protected $_appEmulation;

    /** @var StoreManagerInterface */
    protected $_storeManager;

    protected $_configHelper;

    public function __construct(
        ScopeConfigInterface $scopeConfigInterface,
        ObjectManagerInterface $objectManager,
        Emulation $appEmulation,
        StoreManagerInterface $storeManager,
        Config $configHelper,
        array $data = []
    ) {
        $this->_scopeConfig   = $scopeConfigInterface;
        $this->_objectManager = $objectManager;
        $this->_appEmulation  = $appEmulation;
        $this->_storeManager  = $storeManager;
        $this->_configHelper  = $configHelper;
        parent::__construct($data);
    }

    protected function _getReferenceObject()
    {
        if ($this->_referenceObject === null) {
            $this->_referenceObject = false;
            if ($this->_referenceModel && $this->getReferenceId()) {
                $this->_referenceObject = $this->_objectManager->create(
                    $this->_referenceModel
                );
                $this->_referenceObject->load(
                    $this->getReferenceId()
                );
            }
        }

        return $this->_referenceObject;
    }

    protected function _getChanel()
    {
        $key = $this->getEventKey();
        if ($this->_configHelper->getValue($key, 'send_type') == 'custom') {
            return $this->_configHelper->getValue($key, 'custom_channel');
        }
        return null;
    }

    public function prepareData()
    {

        $referenceObject = $this->_getReferenceObject();
        $store           = $this->_storeManager->getDefaultStoreView();
        if ($referenceObject && $referenceObject->getStoreId()) {
            $store = $this->_storeManager->getStore(
                $referenceObject->getStoreId()
            );
        }

        //Start environment emulation of the specified store
        $this->_appEmulation->startEnvironmentEmulation($store->getId());

        $data             = [
            'channel'     => $this->_getChanel(),
            'attachments' => [],
        ];
        $data['username'] = $store->getName();

        if ($this->_configHelper->getValue('icon', 'general')) {
            $data['icon_url']
                = $this->_storeManager
                      ->getStore()
                      ->getBaseUrl(UrlInterface::URL_TYPE_MEDIA)
                  . 'moogento/slack/'
                  . $this->_configHelper->getValue('icon', 'general');
        } else {
            $data['icon_url'] = $this->_storeManager
                                    ->getStore()
                                    ->getBaseUrl(UrlInterface::URL_TYPE_MEDIA)
                                . 'moogento/slack/moogento_logo_small.png';
        }

        $data['text'] = $this->_prepareText();

        $data['attachments'] = [$this->_prepareAttachments()];

        $this->_appEmulation->stopEnvironmentEmulation();

        return $data;
    }

    protected function _prepareAttachments()
    {
        $key         = $this->getEventKey();
        $attachments = $this->_getAttachments();
        if ($this->_configHelper->getValue($key, 'colorize')) {
            $attachments['color'] = $this->_configHelper
                ->getValue($key, 'color');
        }
        $attachments['fallback'] = $this->_prepareFallback($attachments);

        return $attachments;
    }

    protected function _getAttachments()
    {
        return [];
    }

    protected function _prepareFallback($attachments)
    {
        $fallback = [];
        foreach ($attachments['fields'] as $field) {
            $fallback[] = $field['title'] . ': ' . $field['value'];
        }

        return implode("\n", $fallback);
    }

    public function getEventKey()
    {
        return $this->getData(self::FIELD_EVENT_KEY);
    }

    /**
     * Get additional data
     *
     * @return string|null
     */
    public function getAdditionalData()
    {
        return $this->getData(self::FIELD_ADDITIONAL_DATA);
    }

    /**
     * Get status
     *
     * @return int|null
     */
    public function getStatus()
    {
        return $this->getData(self::FIELD_STATUS);
    }

    /**
     * Get status message
     *
     * @return string|null
     */
    public function getStatusMessage()
    {
        return $this->getData(self::FIELD_STATUS_MESSAGE);
    }

    /**
     * Get date
     *
     * @return string|null
     */
    public function getDate()
    {
        return $this->getData(self::FIELD_DATE);
    }

    /**
     * Get cron id
     *
     * @return string|null
     */
    public function getCronId()
    {
        return $this->getData(self::FIELD_CRON_ID);
    }

    /**
     * Set event key
     *
     * @param $eventKey
     *
     * @return NotificationInterface
     */
    public function setEventKey($eventKey)
    {
        $this->setData(self::FIELD_EVENT_KEY, $eventKey);
        return $this;
    }

    /**
     * Set additional data
     *
     * @param $additionalData
     *
     * @return NotificationInterface
     */
    public function setAdditionalData($additionalData)
    {
        $this->setData(self::FIELD_ADDITIONAL_DATA, $additionalData);
        return $this;
    }

    /**
     * Set status
     *
     * @param $status
     *
     * @return NotificationInterface
     */
    public function setStatus($status)
    {
        $this->setData(self::FIELD_STATUS, $status);
        return $this;
    }

    /**
     * Set status message
     *
     * @param $statusMessage
     *
     * @return NotificationInterface
     */
    public function setStatusMessage($statusMessage)
    {
        $this->setData(self::FIELD_STATUS_MESSAGE, $statusMessage);
        return $this;
    }

    /**
     * Set date
     *
     * @param $date
     *
     * @return NotificationInterface
     */
    public function setDate($date)
    {
        $this->setData(self::FIELD_DATE, $date);
        return $this;
    }

    /**
     * Set cron id
     *
     * @param $cronId
     *
     * @return NotificationInterface
     */
    public function setCronId($cronId)
    {
        $this->setData(self::FIELD_CRON_ID, $cronId);
        return $this;
    }

    /**
     * Get reference id
     *
     * @return int|null
     */
    public function getReferenceId()
    {
        return $this->getData(self::FIELD_REFERENCE_ID);
    }

    /**
     * Set reference id
     *
     * @param $referenceId
     *
     * @return NotificationInterface
     */
    public function setReferenceId($referenceId)
    {
        $this->setData(self::FIELD_REFERENCE_ID, $referenceId);
        return $this;
    }

    abstract protected function _prepareText();
}
