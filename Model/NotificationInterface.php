<?php
namespace Moogento\SlackCommerce\Model;

interface NotificationInterface
{
    const FIELD_EVENT_KEY = 'event_key';
    const FIELD_REFERENCE_ID = 'reference_id';
    const FIELD_ADDITIONAL_DATA = 'additional_data';
    const FIELD_STATUS = 'status';
    const FIELD_STATUS_MESSAGE = 'status_message';
    const FIELD_DATE = 'date';
    const FIELD_CRON_ID = 'cron_id';

    /**
     * Get event key
     *
     * @return string|null
     */
    public function getEventKey();

    /**
     * Get reference id
     *
     * @return int|null
     */
    public function getReferenceId();

    /**
     * Get additional data
     *
     * @return string|null
     */
    public function getAdditionalData();

    /**
     * Get status
     *
     * @return int|null
     */
    public function getStatus();

    /**
     * Get status message
     *
     * @return string|null
     */
    public function getStatusMessage();

    /**
     * Get date
     *
     * @return string|null
     */
    public function getDate();

    /**
     * Get cron id
     *
     * @return string|null
     */
    public function getCronId();

    /**
     * Set event key
     *
     * @param $eventKey
     *
     * @return NotificationInterface
     */
    public function setEventKey($eventKey);

    /**
     * Set reference id
     *
     * @param $referenceId
     *
     * @return NotificationInterface
     */
    public function setReferenceId($referenceId);

    /**
     * Set additional data
     *
     * @param $additionalData
     *
     * @return NotificationInterface
     */
    public function setAdditionalData($additionalData);

    /**
     * Set status
     *
     * @param $status
     *
     * @return NotificationInterface
     */
    public function setStatus($status);

    /**
     * Set status message
     *
     * @param $statusMessage
     *
     * @return NotificationInterface
     */
    public function setStatusMessage($statusMessage);

    /**
     * Set date
     *
     * @param $date
     *
     * @return NotificationInterface
     */
    public function setDate($date);

    /**
     * Set cron id
     *
     * @param $cronId
     *
     * @return NotificationInterface
     */
    public function setCronId($cronId);
}
