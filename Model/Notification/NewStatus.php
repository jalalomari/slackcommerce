<?php
namespace Moogento\SlackCommerce\Model\Notification;

use Magento\Sales\Model\Order\Config;
use Moogento\SlackCommerce\Model\Queue;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\Data\Collection\AbstractDb;
use Magento\Framework\Model\Context;
use Magento\Framework\Model\ResourceModel\AbstractResource;
use Magento\Framework\ObjectManagerInterface;
use Magento\Framework\Registry;
use Magento\Store\Model\App\Emulation;
use Magento\Store\Model\StoreManagerInterface;

class NewStatus extends NewOrder
{
    protected $_orderConfig;

    public function __construct(
        ScopeConfigInterface $scopeConfigInterface,
        ObjectManagerInterface $objectManager,
        Emulation $appEmulation,
        StoreManagerInterface $storeManager,
        Config $orderConfig,
        array $data = []
    ) {
        $this->_orderConfig = $orderConfig;
        parent::__construct(
            $scopeConfigInterface,
            $objectManager,
            $appEmulation,
            $storeManager,
            $data
        );
    }

    protected function _prepareText()
    {
        $status = str_replace(
            Queue::KEY_NEW_STATUS . '_',
            '',
            $this->getEventKey()
        );
        $statuses = $this->_orderConfig->getStatuses();
        if (isset($statuses[$status])) {
            $status = $statuses[$status];
        }
        return __(
            'Order #%1 : %2',
            $this->_getOrder()->getIncrementId(),
            $status
        );
    }
}
