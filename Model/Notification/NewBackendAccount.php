<?php
namespace Moogento\SlackCommerce\Model\Notification;

use Moogento\SlackCommerce\Model\NotificationAbstract;

class NewBackendAccount extends NotificationAbstract
{
    protected $_referenceModel = '\Magento\User\Model\User';

    protected function _prepareText()
    {
        return __('New backend account');
    }

    protected function _getAttachments()
    {
        return [
            'fields' => [
                [
                    'title' => __('User'),
                    'value' =>
                        $this->_getReferenceObject()->getFirstName() . ' '
                        . $this->_getReferenceObject()
                               ->getLastName(),
                    'short' => true,
                ],
                [
                    'title' => __('Email'),
                    'value' => $this->_getReferenceObject()->getEmail(),
                    'short' => true,
                ],
                [
                    'title' => __('Role'),
                    'value' => $this->_getReferenceObject()
                                    ->getRole()
                                    ->getRoleName(),
                    'short' => true,
                ],
            ],
        ];
    }
}
