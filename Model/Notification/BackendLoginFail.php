<?php
namespace Moogento\SlackCommerce\Model\Notification;

use Moogento\SlackCommerce\Model\NotificationAbstract;

class BackendLoginFail extends NotificationAbstract
{
    protected function _prepareText()
    {
        return __('Backend login fail');
    }

    protected function _getChanel()
    {
        if ($this->_configHelper->getSecurityValue('immediate_send_type')
            == 'custom'
        ) {
            return $this->_scopeConfig->getValue(
                'moogento_slackcommerce/security/immediate_custom_channel'
            );
        }
        return null;
    }

    protected function _prepareAttachments()
    {
        $attachments = $this->_getAttachments();
        if ($this->_scopeConfig->getValue(
            'moogento_slackcommerce/security/immediate_colorize'
        )
        ) {
            $attachments['color'] = $this->_scopeConfig->getValue(
                'moogento_slackcommerce/security/immediate_color'
            );
        }
        $attachments['fallback'] = $this->_prepareFallback($attachments);

        return $attachments;
    }

    protected function _getAttachments()
    {
        $additionalData = $this->getAdditionalData();
        if (!is_array($additionalData)) {
            try {
                $additionalData = unserialize($additionalData);
            } catch (\Exception $e) {
                return [];
            }
        }
        $ipFails = $this->_objectManager->create(
            '\Moogento\SlackCommerce\Model\Fails\Ip'
        )
                                        ->loadByIp($additionalData['IP']);
        return [
            'fields' => [
                [
                    'title' => __('User'),
                    'value' => $additionalData['username'],
                    'short' => true,
                ],
                [
                    'title' => __('IP'),
                    'value' => $additionalData['IP']
                               . ($ipFails->getContry() ?
                            ' (' . $ipFails->getContry() . ')' : ''),
                    'short' => false,
                ],
                [
                    'title' => __('Source'),
                    'value' => $additionalData['URL'],
                    'short' => false,
                ],
                [
                    'title' => __('# Fails (this IP)'),
                    'value' => $ipFails->getFails(),
                    'short' => false,
                ],
                [
                    'title' => __('Error'),
                    'value' => $additionalData['message'],
                    'short' => false,
                ],
            ],
        ];
    }
}
