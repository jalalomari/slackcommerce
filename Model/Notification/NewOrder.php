<?php
namespace Moogento\SlackCommerce\Model\Notification;

use Moogento\SlackCommerce\Model\NotificationAbstract;

class NewOrder extends NotificationAbstract
{
    protected $_referenceModel = '\Magento\Sales\Model\Order';

    protected function _prepareText()
    {
        return __('Order #%1', $this->_getOrder()->getIncrementId());
    }

    /**
     * @return \Magento\Sales\Model\Order
     */
    protected function _getOrder()
    {
        return $this->_getReferenceObject();
    }

    protected function _getAttachments()
    {
        $fields = $this->_prepareOrderFields();
        return [
            'fields' => $fields
        ];
    }

    protected function _prepareCustomerName()
    {
        $customerName = $this->_getOrder()->getCustomerName();

        if ($this->_getOrder()->getCustomerIsGuest()
            || ($customerName == __('Guest'))
        ) {
            $customerType = __('Guest');
            $customerName = $this->_getOrder()->getBillingAddress()->getName();
        } else {
            $customerType = __('Customer');
            $customerName = $this->_getOrder()->getCustomerName();
        }

        return [
            'title' => $customerType,
            'value' => $customerName,
            'short' => true,
        ];
    }

    protected function _prepareOrderAmount()
    {
        return [
            'title' => __('Order Amount'),
            'value' => $this->_trimZeros(
                strip_tags(
                    $this->_getOrder()->formatPrice(
                        $this->_getOrder()->getGrandTotal()
                    )
                )
            ),
            'short' => true,
        ];
    }

    protected function _prepareProductsData()
    {
        return [
            'title' => __('Products'),
            'value' => $this->_getProductsData(),
            'short' => true,
        ];
    }

    protected function _trimZeros($amount)
    {
        return preg_replace('~\.00$~', '', $amount);
    }

    protected function _prepareOrderFields()
    {
        return [
            $this->_prepareCustomerName(),
            $this->_prepareOrderAmount(),
            $this->_prepareProductsData(),
        ];
    }

    protected function _getProductsData()
    {
        $data = [];
        $limit = 2;
        $count = count($this->_getOrder()->getAllVisibleItems());
        $i = 0;
        /** @var \Magento\Sales\Model\Order\Item $item */
        foreach ($this->_getOrder()->getAllVisibleItems() as $item) {
            if ($i >= ($limit + 1)) {
                break;
            }
            $data[] = floatval($item->getQtyOrdered() - $item->getQtyCanceled())
                      . ' x ' . $item->getSku();
            $i++;
        }
        if ($count > $i) {
            $data[] = '(+ ' . ($count - $i) . ' more)';
        }

        return implode("\n", $data);
    }
}
