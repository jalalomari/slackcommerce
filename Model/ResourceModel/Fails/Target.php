<?php
namespace Moogento\SlackCommerce\Model\ResourceModel\Fails;

use Magento\Framework\Model\ResourceModel\Db\AbstractDb;

class Target extends AbstractDb
{

    /**
     * Resource initialization
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('moogento_slackcommerce_fails_target', 'id');
    }
}
