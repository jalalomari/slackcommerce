<?php
namespace Moogento\SlackCommerce\Model\ResourceModel\Fails;

use Magento\Framework\Model\ResourceModel\Db\AbstractDb;

class Ip extends AbstractDb
{

    /**
     * Resource initialization
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('moogento_slackcommerce_fails_ip', 'id');
    }

    // @codingStandardsIgnoreStart
    /**
     * @param string                                 $field
     * @param mixed                                  $value
     * @param \Magento\Framework\Model\AbstractModel $object
     *
     * @return \Magento\Framework\DB\Select
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    protected function _getLoadSelect($field, $value, $object)
    {
        $field  = $this->getConnection()->quoteIdentifier(
            sprintf('%s.%s', $this->getMainTable(), $field)
        );
        $select = $this->getConnection()->select()
                       ->from(
                           $this->getMainTable(),
                           [
                               '*',
                               'ip_string' => new \Zend_Db_Expr(
                                   'INET6_NTOA(ip)'
                               )
                           ]
                       )
                       ->where($field . '=?', $value);
        return $select;
    }
    // @codingStandardsIgnoreEnd
}
