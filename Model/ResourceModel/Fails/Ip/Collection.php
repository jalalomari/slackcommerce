<?php
namespace Moogento\SlackCommerce\Model\ResourceModel\Fails\Ip;

use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;

class Collection extends AbstractCollection
{
    protected function _construct()
    {
        $this->_init(
            'Moogento\SlackCommerce\Model\Fails\Ip',
            'Moogento\SlackCommerce\Model\ResourceModel\Fails\Ip'
        );
    }
}
