<?php
namespace Moogento\SlackCommerce\Model\ResourceModel\Queue;

use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;

class Collection extends AbstractCollection
{
    protected function _construct()
    {
        $this->_init(
            'Moogento\SlackCommerce\Model\Queue',
            'Moogento\SlackCommerce\Model\ResourceModel\Queue'
        );
    }
}
