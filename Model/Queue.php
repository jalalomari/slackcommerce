<?php
namespace Moogento\SlackCommerce\Model;

use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\Data\Collection\AbstractDb;
use Magento\Framework\Model\AbstractModel;
use Magento\Framework\Model\Context;
use Magento\Framework\Model\ResourceModel\AbstractResource;
use Magento\Framework\ObjectManagerInterface;
use Magento\Framework\Registry;
use Moogento\SlackCommerce\Helper\Api;
use Moogento\SlackCommerce\Helper\Config;

class Queue extends AbstractModel implements NotificationInterface
{
    const KEY_NEW_ORDER = 'NewOrder';
    const KEY_NEW_INVOICE = 'NewInvoice';
    const KEY_NEW_SHIPMENT = 'NewShipment';
    const KEY_NEW_CREDIT = 'NewCredit';
    const KEY_NEW_STATUS = 'NewStatus';
    const KEY_NEW_BACKEND_ACCOUNT = 'NewBackendAccount';
    const KEY_BACKEND_LOGIN = 'BackendLogin';
    const KEY_BACKEND_LOGIN_FAIL = 'BackendLoginFail';

    const STATUS_QUEUED = 0;
    const STATUS_SUCCESS = 1;
    const STATUS_ERROR = 2;

    protected $_apiHelper;
    protected $_configHelper;
    protected $_scopeConfig;
    protected $_objectManager;

    public function __construct(
        Context $context,
        Registry $registry,
        Api $apiHelper,
        Config $configHelper,
        ScopeConfigInterface $scopeConfigInterface,
        ObjectManagerInterface $objectManager,
        AbstractResource $resource = null,
        AbstractDb $resourceCollection = null,
        array $data = []
    ) {
        $this->_apiHelper     = $apiHelper;
        $this->_configHelper  = $configHelper;
        $this->_scopeConfig   = $scopeConfigInterface;
        $this->_objectManager = $objectManager;
        parent::__construct(
            $context,
            $registry,
            $resource,
            $resourceCollection,
            $data
        );
    }

    protected function _construct()
    {
        $this->_init('Moogento\SlackCommerce\Model\ResourceModel\Queue');
    }

    public function send()
    {
        $notification = $this->_getNotification();

        $data = $notification->prepareData();
        if ($notification->getEventKey() == self::KEY_BACKEND_LOGIN_FAIL) {
            if ($this->_configHelper->getSecurityValue('send_type_immediate')
                == Config::SEND_TYPE_CUSTOM
            ) {
                if ($this->_configHelper->getSecurityValue('custom_channel')
                ) {
                    $data['channel'] = $this->_configHelper->getSecurityValue(
                        'immediate_custom_channel'
                    );
                }
            }
            if ($this->_configHelper->getSecurityValue('colorize_immediate')) {
                foreach ($data["attachments"] as $index => $value) {
                    $data["attachments"][$index]['color']
                        = $this->_configHelper
                            ->getSecurityValue('color_immediate');
                }
            }
        }

        try {
            $result = $this->_apiHelper->send($data);
            if ($result === true) {
                $this->setStatus(self::STATUS_SUCCESS);
            } else {
                $this->setStatus(self::STATUS_ERROR);
                $this->setStatusMessage($result);
            }
        } catch (\Exception $e) {
            $this->setStatus(self::STATUS_ERROR);
            $this->setStatusMessage($e->getMessage());
        }
        $this->save();
    }

    protected function _getNotification()
    {
        if (strpos($this->getEventKey(), self::KEY_NEW_STATUS) === 0) {
            $alias = 'Moogento\SlackCommerce\Model\Notification\\'
                     . self::KEY_NEW_STATUS;
        } else {
            $alias = 'Moogento\SlackCommerce\Model\Notification\\'
                     . $this->getEventKey();
        }

        $model = $this->_objectManager->create($alias);
        $model->setData($this->getData());

        return $model;
    }

    /**
     * Get event key
     *
     * @return string|null
     */
    public function getEventKey()
    {
        return $this->getData(self::FIELD_EVENT_KEY);
    }

    /**
     * Get additional data
     *
     * @return string|null
     */
    public function getAdditionalData()
    {
        return $this->getData(self::FIELD_ADDITIONAL_DATA);
    }

    /**
     * Get status
     *
     * @return int|null
     */
    public function getStatus()
    {
        return $this->getData(self::FIELD_STATUS);
    }

    /**
     * Get status message
     *
     * @return string|null
     */
    public function getStatusMessage()
    {
        return $this->getData(self::FIELD_STATUS_MESSAGE);
    }

    /**
     * Get date
     *
     * @return string|null
     */
    public function getDate()
    {
        return $this->getData(self::FIELD_DATE);
    }

    /**
     * Get cron id
     *
     * @return string|null
     */
    public function getCronId()
    {
        return $this->getData(self::FIELD_CRON_ID);
    }

    /**
     * Set event key
     *
     * @param $eventKey
     *
     * @return NotificationInterface
     */
    public function setEventKey($eventKey)
    {
        $this->setData(self::FIELD_EVENT_KEY, $eventKey);
        return $this;
    }

    /**
     * Set additional data
     *
     * @param $additionalData
     *
     * @return NotificationInterface
     */
    public function setAdditionalData($additionalData)
    {
        $this->setData(self::FIELD_ADDITIONAL_DATA, $additionalData);
        return $this;
    }

    /**
     * Set status
     *
     * @param $status
     *
     * @return NotificationInterface
     */
    public function setStatus($status)
    {
        $this->setData(self::FIELD_STATUS, $status);
        return $this;
    }

    /**
     * Set status message
     *
     * @param $statusMessage
     *
     * @return NotificationInterface
     */
    public function setStatusMessage($statusMessage)
    {
        $this->setData(self::FIELD_STATUS_MESSAGE, $statusMessage);
        return $this;
    }

    /**
     * Set date
     *
     * @param $date
     *
     * @return NotificationInterface
     */
    public function setDate($date)
    {
        $this->setData(self::FIELD_DATE, $date);
        return $this;
    }

    /**
     * Set cron id
     *
     * @param $cronId
     *
     * @return NotificationInterface
     */
    public function setCronId($cronId)
    {
        $this->setData(self::FIELD_CRON_ID, $cronId);
        return $this;
    }

    /**
     * Get reference id
     *
     * @return int|null
     */
    public function getReferenceId()
    {
        $this->getData(self::FIELD_REFERENCE_ID);
    }

    /**
     * Set reference id
     *
     * @param $referenceId
     *
     * @return NotificationInterface
     */
    public function setReferenceId($referenceId)
    {
        $this->setData(self::FIELD_REFERENCE_ID, $referenceId);
        return $this;
    }
}
