<?php
namespace Moogento\SlackCommerce\Model\Fails;

use Magento\Framework\Data\Collection\AbstractDb;
use Magento\Framework\Model\AbstractModel;
use Magento\Framework\Model\ResourceModel\AbstractResource;
use Moogento\SlackCommerce\Helper\Data;

class Ip extends AbstractModel
{
    protected $_helper;

    public function __construct(
        \Magento\Framework\Model\Context $context,
        \Magento\Framework\Registry $registry,
        Data $helper,
        AbstractResource $resource = null,
        AbstractDb $resourceCollection = null,
        array $data = []
    ) {
        $this->_helper = $helper;
        parent::__construct(
            $context,
            $registry,
            $resource,
            $resourceCollection,
            $data
        );
    }

    protected function _construct()
    {
        $this->_init('Moogento\SlackCommerce\Model\ResourceModel\Fails\Ip');
    }

    public function loadByIp($ip)
    {
        $this->getResource()->load(
            $this,
            new \Zend_Db_Expr(
                'INET6_ATON("' . $ip . '")'
            ),
            'ip'
        );
        return $this;
    }

    public function getCountry()
    {
        if ($this->getData('country') === null) {
            $this->setData(
                'country',
                $this->_helper->getCountryByIp(
                    $this->getData('ip_string')
                )
            );
            $this->save();
        }

        return $this->getData('country');
    }
}
