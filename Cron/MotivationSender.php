<?php
namespace Moogento\SlackCommerce\Cron;

use Magento\Framework\App\ResourceConnection;
use Magento\Framework\Module\Dir;
use Moogento\SlackCommerce\Helper\Config;

class MotivationSender extends CronAbstract
{
    /** @var \Magento\Framework\Module\Dir\Reader */
    protected $_moduleReader;

    /** @var \Magento\Directory\Model\Currency */
    protected $_baseCurrency;

    public function __construct(
        CronContext $context,
        \Magento\Framework\Module\Dir\Reader $moduleReader,
        \Magento\Directory\Model\CurrencyFactory $currencyFactory
    ) {
        $this->_moduleReader = $moduleReader;
        $baseCurrencyCode    = (string) $context->getStoreManager()
                                                ->getStore()
                                                ->getBaseCurrencyCode();
        $this->_baseCurrency = $currencyFactory->create()->load(
            $baseCurrencyCode
        );
        parent::__construct($context);
    }

    public function execute()
    {
        $thisWeek = $this->_context->getDateTime()->date(
            'Y-m-d H:00:00',
            '-7days'
        );
        $lastWeek = $this->_context->getDateTime()->date(
            'Y-m-d H:00:00',
            '-14days'
        );

        $qtyCanceledExpr = $this->_getIfNullSql('qty_canceled', 0);
        $qtyRefundedExpr = $this->_getIfNullSql('qty_refunded', 0);
        $selectValue     = new \Zend_Db_Expr(
            "sum((qty_ordered - {$qtyCanceledExpr} - {$qtyRefundedExpr})" .
                           " * base_price)"
        );

        $select = $this->_context->getConnection()->select();
        $select->from(
            [
                'o' => $this->_context->getResource()->getTableName(
                    'sales_order_item'
                )
            ],
            ['value' => $selectValue]
        );
        $select->where('o.created_at >= ?', $thisWeek);
        $thisWeekValue = (double) $this->_context->getConnection()->fetchOne(
            $select
        );

        $select = $this->_context->getConnection()->select();
        $select->from(
            [
                'o' => $this->_context->getResource()->getTableName(
                    'sales_order_item'
                )
            ],
            ['value' => $selectValue]
        );
        $select->where('o.created_at >= ?', $lastWeek);
        $select->where('o.created_at < ?', $thisWeek);

        $lastWeekValue = (double) $this->_context->getConnection()->fetchOne(
            $select
        );

        $difference = $thisWeekValue - $lastWeekValue;

        $moduleEtcDir  = $this->_moduleReader->getModuleDir(
            Dir::MODULE_ETC_DIR,
            'Moogento_SlackCommerce'
        );
        $directoryRead = $this->_context->getReadFactory()->create(
            $moduleEtcDir
        );

        $weeksChange = (int) $this->_context->getConfigHelper()->getValue(
            Config::KEY_WEEKS_CHANGE,
            false,
            Config::SECTION_GENERAL
        );
        if ($difference > 0) {
            $file = 'positive.txt';
            if ($weeksChange > 0) {
                ++$weeksChange;
            } else {
                $weeksChange = 1;
            }
        } elseif ($difference < 0) {
            $file = 'negative.txt';
            if ($weeksChange < 0) {
                --$weeksChange;
            } else {
                $weeksChange = -1;
            }
        } else {
            $file        = 'neutral.txt';
            $weeksChange = 0;
        }
        $this->_context->getConfigHelper()->save(
            $weeksChange,
            Config::KEY_WEEKS_CHANGE
        );

        if ($directoryRead->isFile($file)) {
            $lines      = preg_split('/\n/i', $directoryRead->readFile($file));
            $resultLine = $lines[rand(0, count($lines) - 1)];

            $weeksChange = abs($weeksChange);
            $difference  = abs($difference);
            $resultLine  = str_replace(
                "$[x]",
                $this->_baseCurrency->formatPrecision(
                    (float) $difference,
                    0,
                    [],
                    false
                ),
                $resultLine
            );
            $resultLine  = str_replace("[x]", $weeksChange, $resultLine);

            $data = $this->_getGeneralData();
            $data['text']
                  = $resultLine . " <https://moogento.com|Powered by Moogento>";

            try {
                $this->_context->getApiHelper()->send($data);
            } catch (\Exception $e) {
                $this->_context->getLogger()->critical($e);
            }
        }
    }
}
