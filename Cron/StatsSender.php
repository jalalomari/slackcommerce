<?php
namespace Moogento\SlackCommerce\Cron;

use Moogento\SlackCommerce\Helper\Config;

class StatsSender extends CronAbstract
{
    /** @var CurrencyInterface */
    protected $_localeCurrency;

    /** @var \Magento\Directory\Model\Currency */
    protected $_baseCurrency;

    protected $_statFields
        = [
            Config::KEY_QTY_ORDERS         => '# Orders',
            Config::KEY_TOTAL_REVENUE      => '__CUR__ Revenue',
            Config::KEY_QTY_PRODUCTS       => '# Products',
            Config::KEY_AVG_PRODUCTS_ORDER => '# Prod\'s /Order',
            Config::KEY_AVG_REVENUE_ORDER  => '__CUR__ Rev /Order',
        ];

    public function __construct(
        CronContext $context,
        \Magento\Framework\Locale\CurrencyInterface $localeCurrency,
        \Magento\Directory\Model\CurrencyFactory $currencyFactory
    ) {
        $this->_localeCurrency = $localeCurrency;
        $baseCurrencyCode      = (string) $context->getStoreManager()->getStore(
        )->getBaseCurrencyCode();

        $this->_baseCurrency = $currencyFactory->create()->load(
            $baseCurrencyCode
        );
        parent::__construct($context);
    }

    public function execute()
    {
        if (!$this->_context->getConfigHelper()->getWebHookUrl()) {
            return;
        }

        if ($this->_context->getConfigHelper()->shouldSend(
            Config::KEY_STATS,
            Config::SECTION_STATS
        )
        ) {
            if ($this->_context->getDateTime()->date('G')
                != $this->_context->getConfigHelper()->getValue(
                    Config::KEY_HOUR,
                    false,
                    Config::SECTION_STATS
                )
            ) {
                return;
            }

            if ($this->_context->getConfigHelper()->isSetFlag(
                Config::KEY_DAILY_STATS,
                false,
                Config::SECTION_STATS
            )
            ) {
                $this->_sendDailyStats();
            }

            if ($this->_context->getConfigHelper()->isSetFlag(
                Config::KEY_WEEKLY_STATS,
                false,
                Config::SECTION_STATS
            )
            ) {
                if ($this->_context->getDateTime()->date('w')
                    == $this->_context->getConfigHelper()->getValue(
                        Config::KEY_DAY,
                        false,
                        Config::SECTION_STATS
                    )
                ) {
                    $this->_sendWeeklyStats();
                }
            }
        }
    }

    protected function _sendDailyStats()
    {
        $data = $this->_getGeneralData();

        $data['text']        = __(
            'Daily Metrics for %1',
            $this->_getMagentoDate()
        );
        $data['attachments'] = [$this->_prepareAttachments('24hours')];

        try {
            $this->_context->getApiHelper()->send($data);
        } catch (\Exception $e) {
            $this->_context->getLogger()->critical($e);
        }
    }

    protected function _sendWeeklyStats()
    {
        $data                = $this->_getGeneralData();
        $data['text']        = __(
            'Weekly Metrics for week ending %1',
            $this->_getMagentoDate()
        );
        $data['attachments'] = [$this->_prepareAttachments('7days')];

        try {
            $this->_context->getApiHelper()->send($data);
        } catch (\Exception $e) {
            $this->_context->getLogger()->critical($e);
        }
    }

    protected function _prepareAttachments($period)
    {
        $data = [];
        if ($this->_context->getConfigHelper()
                           ->isSetFlag(
                               Config::KEY_STATS,
                               Config::SUBTYPE_COLORIZE,
                               Config::SECTION_STATS
                           )
        ) {
            $data['color'] = $this->_context->getConfigHelper()->getValue(
                Config::KEY_STATS,
                Config::SUBTYPE_COLOR,
                Config::SECTION_STATS
            );
        }

        $baseCurrencySymbol = $this->_baseCurrency->getCurrencySymbol();

        $fields = [];
        foreach ($this->_statFields as $field => $label) {
            $label = str_replace('__CUR__', $baseCurrencySymbol, $label);
            if ($this->_context->getConfigHelper()->isSetFlag(
                $field,
                false,
                Config::SECTION_STATS
            )
            ) {
                $fields[] = [
                    'title' => __($label),
                    'value' => $this->_getFieldValue($field, $period),
                    'short' => true,
                ];
            }
        }

        $data['fields'] = $fields;

        return $data;
    }

    protected function _getFieldValue($field, $period)
    {
        $fieldExpression = null;
        $formatPrice     = false;
        $joinItems       = false;

        switch ($field) {
            case Config::KEY_QTY_ORDERS:
                $fieldExpression = 'count(DISTINCT entity_id)';
                break;
            case 'total_revenue':
                $formatPrice     = true;
                $fieldExpression = sprintf(
                    'SUM((%s - %s - %s - (%s - %s - %s)) * %s)',
                    $this->_getIfNullSql('o.base_total_invoiced', 0),
                    $this->_getIfNullSql('o.base_tax_invoiced', 0),
                    $this->_getIfNullSql('o.base_shipping_invoiced', 0),
                    $this->_getIfNullSql('o.base_total_refunded', 0),
                    $this->_getIfNullSql('o.base_tax_refunded', 0),
                    $this->_getIfNullSql('o.base_shipping_refunded', 0),
                    $this->_getIfNullSql('o.base_to_global_rate', 0)
                );
                break;
            case Config::KEY_QTY_PRODUCTS:
                $joinItems       = true;
                $fieldExpression = 'SUM(oi.total_qty_ordered)';
                break;
            case Config::KEY_AVG_PRODUCTS_ORDER:
                $joinItems = true;
                $fieldExpression
                           = 'SUM(oi.total_qty_ordered)'
                             . '/COUNT(DISTINCT o.entity_id)';
                break;
            case Config::KEY_AVG_REVENUE_ORDER:
                $formatPrice     = true;
                $fieldExpression = sprintf(
                    'SUM((%s - %s - %s - (%s - %s - %s)) * %s) '.
                                   '/ count(DISTINCT entity_id)',
                    $this->_getIfNullSql('o.base_total_invoiced', 0),
                    $this->_getIfNullSql('o.base_tax_invoiced', 0),
                    $this->_getIfNullSql('o.base_shipping_invoiced', 0),
                    $this->_getIfNullSql('o.base_total_refunded', 0),
                    $this->_getIfNullSql('o.base_tax_refunded', 0),
                    $this->_getIfNullSql('o.base_shipping_refunded', 0),
                    $this->_getIfNullSql('o.base_to_global_rate', 0)
                );
                break;
        }

        $orderTable     = $this->_context->getResource()->getTableName(
            'sales_order'
        );
        $orderItemTable = $this->_context->getResource()->getTableName(
            'sales_order_item'
        );

        $date = $this->_context->getDateTime()->gmtDate(
            'Y-m-d H:00:00',
            '-' . $period
        );

        $select = $this->_context->getConnection()->select();
        $select->from(
            ['o' => $orderTable],
            [
                'value' => new \Zend_Db_Expr($fieldExpression),
            ]
        );
        if ($joinItems) {
            $qtyCanceledExpr = $this->_getIfNullSql('qty_canceled', 0);
            $cols            = [
                'order_id'            => 'order_id',
                'total_qty_ordered'   => new \Zend_Db_Expr(
                    "SUM(qty_ordered - {$qtyCanceledExpr})"
                ),
                'total_product_price' => new \Zend_Db_Expr("SUM(base_price)"),
                'items_count'         => new \Zend_Db_Expr("COUNT(1)"),
            ];
            $selectOrderItem = $this->_context->getConnection()->select();
            $selectOrderItem->from($orderItemTable, $cols)
                            ->where('parent_item_id IS NULL')
                            ->group('order_id');

            $select->joinLeft(
                ['oi' => $selectOrderItem],
                'o.entity_id = oi.order_id'
            );
        }

        $select->where('o.created_at >= ?', $date);

        $value = $this->_context->getConnection()->fetchOne($select);
        if ($formatPrice) {
            $value = $this->_baseCurrency->formatPrecision(
                (float) $value,
                0,
                [],
                false
            );

        } else {
            $value = round($value);
        }

        return $value;
    }
}
