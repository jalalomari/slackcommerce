<?php
namespace Moogento\SlackCommerce\Cron;

use Magento\Framework\ObjectManagerInterface;
use Magento\Framework\Filesystem;

class CronContext
{
    /** @var \Moogento\SlackCommerce\Helper\Config */
    protected $_configHelper;

    /** @var \Magento\Framework\App\ResourceConnection */
    protected $_resource;

    /** @var \Magento\Framework\DB\Adapter\AdapterInterface */
    protected $_connection;

    /** @var \Magento\Store\Model\App\Emulation */
    protected $_appEmulation;

    /** @var \Magento\Framework\Stdlib\DateTime\DateTime */
    protected $_dateTime;

    /** @var \Magento\Store\Model\StoreManagerInterface */
    protected $_storeManager;

    /** @var \Moogento\SlackCommerce\Helper\Api */
    protected $_apiHelper;

    /** @var \Psr\Log\LoggerInterface */
    protected $_logger;

    /** @var \Magento\Framework\Filesystem */
    protected $_filesystem;

    /** @var  \Magento\Framework\Filesystem\Directory\ReadFactory */
    protected $_readFactory;

    public function __construct(
        \Magento\Framework\App\ResourceConnection $resource,
        \Psr\Log\LoggerInterface $logger,
        ObjectManagerInterface $objectManager,
        \Magento\Framework\Filesystem $filesystem,
        \Magento\Framework\Filesystem\Directory\ReadFactory $readFactory,
        \Magento\Store\Model\App\Emulation $appEmulation,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Moogento\SlackCommerce\Helper\Config $configHelper,
        \Moogento\SlackCommerce\Helper\Api $apiHelper
    ) {
        $this->_resource     = $resource;
        $this->_connection   = $resource->getConnection();
        $this->_appEmulation = $appEmulation;
        $this->_configHelper = $configHelper;
        $this->_storeManager = $storeManager;
        $this->_apiHelper    = $apiHelper;
        $this->_logger       = $logger;
        $this->_dateTime     = $objectManager->get(
            'Magento\Framework\Stdlib\DateTime\DateTime'
        );
        $this->_filesystem   = $filesystem;
        $this->_readFactory  = $readFactory;
    }

    /**
     * @return \Moogento\SlackCommerce\Helper\Config
     */
    public function getConfigHelper()
    {
        return $this->_configHelper;
    }

    /**
     * @return \Magento\Framework\App\ResourceConnection
     */
    public function getResource()
    {
        return $this->_resource;
    }

    /**
     * @return \Magento\Framework\DB\Adapter\AdapterInterface
     */
    public function getConnection()
    {
        return $this->_connection;
    }

    /**
     * @return \Magento\Store\Model\App\Emulation
     */
    public function getAppEmulation()
    {
        return $this->_appEmulation;
    }

    /**
     * @return \Magento\Framework\Stdlib\DateTime\DateTime
     */
    public function getDateTime()
    {
        return $this->_dateTime;
    }

    /**
     * @return \Magento\Store\Model\StoreManagerInterface
     */
    public function getStoreManager()
    {
        return $this->_storeManager;
    }

    /**
     * @return \Moogento\SlackCommerce\Helper\Api
     */
    public function getApiHelper()
    {
        return $this->_apiHelper;
    }

    /**
     * @return \Psr\Log\LoggerInterface
     */
    public function getLogger()
    {
        return $this->_logger;
    }

    /**
     * @return \Magento\Framework\Filesystem
     */
    public function getFileSystem()
    {
        return $this->_filesystem;
    }

    public function getReadFactory()
    {
        return $this->_readFactory;
    }
}
