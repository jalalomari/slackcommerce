<?php
namespace Moogento\SlackCommerce\Cron;

use Magento\Framework\App\ResourceConnection;
use Magento\Framework\ObjectManager\Code\Generator\Factory;
use Magento\Setup\Model\ObjectManagerProvider;
use Magento\Store\Model\App\Emulation;
use Magento\Store\Model\StoreManagerInterface;
use Moogento\SlackCommerce\Helper\Api;
use Moogento\SlackCommerce\Helper\Config;
use Moogento\SlackCommerce\Model\Fails\IpFactory;
use Moogento\SlackCommerce\Model\Fails\TargetFactory;
use Moogento\SlackCommerce\Model\ResourceModel\Fails\Ip\Collection;
use Moogento\SlackCommerce\Model\ResourceModel\Fails\Target\Collection
    as TargetCollection;
use Moogento\SlackCommerce\Setup\SetupContextInterface;
use Psr\Log\LoggerInterface;

class SecuritySender extends CronAbstract
{
    protected $_ipFactory;
    protected $_targetFactory;

    /**
     * @param CronContext   $context
     * @param IpFactory     $ipFactory
     * @param TargetFactory $targetFactory
     *
     */
    public function __construct(
        CronContext $context,
        IpFactory $ipFactory,
        TargetFactory $targetFactory
    ) {
        $this->_ipFactory     = $ipFactory;
        $this->_targetFactory = $targetFactory;
        parent::__construct($context);
    }

    public function execute()
    {
        if (!$this->_context->getConfigHelper()->getWebHookUrl()) {
            return;
        }
        if ($this->_context->getConfigHelper()->shouldSend(
            Config::KEY_REPORT,
            Config::SECTION_SECURITY
        )
        ) {
            if ($this->_context->getDateTime()->date('G')
                != $this->_context->getConfigHelper()->getValue(
                    Config::KEY_HOUR,
                    false,
                    Config::SECTION_SECURITY
                )
            ) {
                return;
            }
            $this->_sendDailyFailStatistic();
        }
    }

    protected function _sendDailyFailStatistic()
    {
        $totalFails = $this->_getTotalNumberOfFails();
        if (!$totalFails
            && $this->_context->getConfigHelper()->isSetFlag(
                Config::KEY_SKIP_NO_FAILS,
                false,
                Config::SECTION_SECURITY
            )
        ) {
            return;
        }

        $data = $this->_getGeneralData();

        $sendType = $this->_context->getConfigHelper()
                                   ->getSendType(
                                       Config::KEY_REPORT,
                                       Config::SECTION_SECURITY
                                   );

        switch ($sendType) {
            case Config::SEND_TYPE_CUSTOM:
                $data['channel'] = $this->_context->getConfigHelper()->getValue(
                    Config::KEY_REPORT,
                    Config::SUBTYPE_CUSTOM_CHANNEL,
                    Config::SECTION_SECURITY
                );
                break;
            default:
                $data['channel'] = $this->_context->getConfigHelper()
                                                  ->getDefaultChannel();
        }
        $data['text']        = __(
            'Daily Security Summary %1',
            $this->_getMagentoDate()
        );
        $data['attachments'] = [];

        $color = false;
        if ($this->_context->getConfigHelper()->isSetFlag(
            Config::KEY_REPORT,
            Config::SUBTYPE_COLORIZE,
            Config::SECTION_SECURITY
        )
        ) {
            $color = $this->_context->getConfigHelper()->getValue(
                Config::KEY_REPORT,
                Config::SUBTYPE_COLOR,
                Config::SECTION_SECURITY
            );
        }

        if ($this->_context->getConfigHelper()->isSetFlag(
            Config::KEY_TOTAL_FAILS,
            false,
            Config::SECTION_SECURITY
        )
        ) {
            $data['attachments'][] = $this->_prepareTotalFails(
                $totalFails,
                $color
            );
        }

        if ($this->_context->getConfigHelper()->isSetFlag(
            Config::KEY_COUNT_IP_FAILS,
            false,
            Config::SECTION_SECURITY
        )
        ) {
            $data['attachments'][] = $this->_prepareCountOfFailsPerIP($color);
        }
        if ($this->_context->getConfigHelper()->isSetFlag(
            Config::KEY_COUNT_TARGET_FAILS,
            false,
            Config::SECTION_SECURITY
        )
        ) {
            $data['attachments'][] = $this->_prepareCountOfFailsPerTarget(
                $color
            );
        }
        try {
            $this->_context->getApiHelper()->send($data);
        } catch (\Exception $e) {
            $this->_context->getLogger()->critical($e);
        }

        $ipFailTable = $this->_context->getResource()->getTableName(
            SetupContextInterface::FAILS_IP_TABLE
        );
        $this->_context->getConnection()->update(
            $ipFailTable,
            ['fails_per_day' => 0],
            '1=1'
        );

        $targetFailTable = $this->_context->getResource()->getTableName(
            SetupContextInterface::FAILS_TARGET_TABLE
        );
        $this->_context->getConnection()->update(
            $targetFailTable,
            ['fails_per_day' => 0],
            '1=1'
        );
    }

    protected function _prepareTotalFails($number, $color)
    {
        $data             = [];
        $data['fallback'] = __("# Fails (all IPs)") . ' ' . $number;
        $data['fields']   = [];
        $data['fields'][] = [
            'title' => __("# Fails (all IPs)"),
            'value' => $number,
            'short' => true,
        ];
        if ($color) {
            $data['color'] = $color;
        }

        return $data;
    }

    protected function _getTotalNumberOfFails()
    {
        $ipFailTable = $this->_context->getResource()->getTableName(
            SetupContextInterface::FAILS_IP_TABLE
        );
        $select      = $this->_context->getConnection()->select();
        $select->from(
            ['o' => $ipFailTable],
            [
                'value' => new \Zend_Db_Expr('sum(fails_per_day)'),
            ]
        );

        return $this->_context->getConnection()->fetchOne($select);
    }

    protected function _prepareCountOfFailsPerIP($color)
    {
        /** @var Collection $collection */
        $collection = $this->_ipFactory->create()->getCollection();
        $collection->addFieldToFilter('fails_per_day', ['neq' => 0]);
        $collection->addFieldToSelect('fails_per_day');
        $collection->addFieldToSelect(
            new \Zend_Db_Expr('INET6_NTOA(ip)'),
            'ip_string'
        );

        $data             = [];
        $data['fallback'] = __("# Fails (by IP)");
        $data['title']    = __("# Fails (by IP)");
        $data['fields']   = [];

        /** @var \Moogento\SlackCommerce\Model\Fails\Ip $row */
        foreach ($collection as $row) {
            $data['fields'][] = [
                'title' => $row->getIpString(),
                'value' => $row->getFailsPerDay(),
                'short' => true,
            ];
        }
        if ($color) {
            $data['color'] = $color;
        }
        return $data;
    }

    protected function _prepareCountOfFailsPerTarget($color)
    {
        /** @var TargetCollection $collection */
        $collection = $this->_targetFactory->create()->getCollection();
        $collection->addFieldToFilter('fails_per_day', ['neq' => 0]);

        $data             = [];
        $data['title']    = __("# Fails (by target)");
        $data['fallback'] = __("# Fails (by target)");
        $data['fields']   = [];

        /** @var \Moogento\SlackCommerce\Model\Fails\Target $row */
        foreach ($collection as $row) {
            $data['fields'][] = [
                'title' => $row->getTarget(),
                'value' => $row->getFailsPerDay(),
                'short' => true,
            ];
        }
        if ($color) {
            $data['color'] = $color;
        }
        return $data;
    }
}
