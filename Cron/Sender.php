<?php
namespace Moogento\SlackCommerce\Cron;

use Moogento\SlackCommerce\Model\ResourceModel\Queue\Collection;
use Moogento\SlackCommerce\Setup\SetupContextInterface;

class Sender extends CronAbstract
{
    /** @var \Moogento\SlackCommerce\Model\QueueFactory */
    protected $_queueFactory;

    public function __construct(
        CronContext $context,
        \Moogento\SlackCommerce\Model\QueueFactory $queueFactory
    ) {
        $this->_queueFactory = $queueFactory;
        parent::__construct($context);
    }

    public function execute()
    {
        if (!$this->_context->getConfigHelper()->getWebHookUrl()) {
            return;
        }

        $limit = 50;
        $table = $this->_context->getResource()->getTableName(
            SetupContextInterface::QUEUE_TABLE
        );

        $this->_context->getConnection()->update(
            $table,
            ['cron_id' => null],
            'status = 0 AND cron_id IS NOT NULL AND DATEDIFF(`date`, NOW()) > 0'
        );

        $cronId = md5(time());
        $query
                = <<<QUERY
UPDATE {$table} SET cron_id = '{$cronId}'
WHERE status = 0 AND cron_id IS NULL LIMIT $limit
QUERY;

        $this->_context->getConnection()->query($query);

        /** @var Collection $collection */
        $collection = $this->_queueFactory->create()->getCollection();
        $collection->addFieldToFilter('cron_id', $cronId);

        /** @var \Moogento\SlackCommerce\Model\Queue $notification */
        foreach ($collection as $notification) {
            $notification->send();
        }
    }
}
