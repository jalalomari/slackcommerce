define(['knockout', './jscolor'], function(ko) {
    ko.bindingHandlers.color = {
        init: function(element) {
            new jscolor(element);
        }
    };
});
