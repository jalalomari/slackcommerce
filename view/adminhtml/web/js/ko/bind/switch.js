define([
    'jquery',
    'knockout',
    './jquery.switchButton'
], function($, ko) {
    ko.bindingHandlers.switch = {
        init: function(element, valueAccessor, allBindings) {
            var $element = $(element);
            $element.switchButton({
                show_labels: false,
                on_callback: function() {
                    var value = valueAccessor();
                    value(1);
                },
                off_callback: function() {
                    var value = valueAccessor();
                    value(0);
                }
            });
            ['disable'].forEach(function(propName) {
                if (allBindings.has(propName)) {
                    var prop = allBindings.get(propName);
                    if (ko.isObservable(prop)) {
                        prop.subscribe(function(newVal) {
                            setTimeout(function() {
                                if (newVal) {
                                    $element.switchButton('instance').disable();
                                } else {
                                    $element.switchButton('instance').enable();
                                }
                            }, 0);
                        });
                    }
                }
            });
        }
    };
});
